\documentclass[aspectratio=169]{beamer}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{sagetex}
\usepackage{ulem}
\usepackage{xcolor}
\usepackage{tcolorbox}
\usepackage{tikz-cd}

\usetikzlibrary{decorations.pathmorphing,shapes}
\usetikzlibrary{arrows.meta}
\tikzcdset{arrow style=tikz,
    squigarrow/.style={
        decoration={
        snake, 
        amplitude=.4mm,
        segment length=2mm
        }, 
        rounded corners=.2pt,
        decorate
        }
    }

\usetheme{edmaths}
\renewcommand\mathfamilydefault{\rmdefault}

\title{Stability of Vector Bundles
\\ to Stability in Triangulated Categories}
\subtitle{Geometric Problem Taking an Algebraic Turn}
\author{Luke Naylor}
\institute{Artin + Gears joint meeting}
\date{September 2023}

\newcommand\RR{\mathbb{R}}
\newcommand\CC{\mathbb{C}}
\newcommand\ZZ{\mathbb{Z}}
\newcommand\QQ{\mathbb{Q}}
\newcommand\centralcharge{\mathcal{Z}}
\newcommand\coh{\operatorname{Coh}}
\newcommand\rank{\operatorname{rk}}
\newcommand\degree{\operatorname{deg}}
\newcommand\realpart{\mathfrak{Re}}
\newcommand\imagpart{\mathfrak{Im}}
\newcommand\bigO{\mathcal{O}}
\newcommand\cohom{\mathcal{H}}
\newcommand\chern{\mathrm{ch}}
\newcommand\Torsion{\mathcal{T}}
\newcommand\Free{\mathcal{F}}
\newcommand\firsttilt[1]{\mathcal{B}^{#1}}
\newcommand\secondtilt[2]{\mathcal{A}^{#1,#2}}
\newcommand\derived{\mathcal{D}}

\begin{document}

\begin{frame}{}
	\titlepage{}
\end{frame}

\section{This Talk}
\begin{frame}{This Talk}
	\begin{itemize}
		\item I work in algebraic geometry (Bridgeland stabilities)
		\item This is an algebra talk
		\item Geometry in original setting but avoided later
	\end{itemize}
\end{frame}

\section{Original Geometric Setting}
\begin{frame}{Purpose of Stability for Vector Bundles}
	\begin{itemize}
		\item Vector Bundles are associated numerical invariants
			(rank, degree, \ldots)
		\item Possible for multiple vector bundles to have same numerical
			invariants:
			\[
				\bigO(n) \oplus \bigO(-n) \qquad \text{on curves}
			\]
		\item Use Stability to parametrize vector bundles of same invariants
			\begin{itemize}
				\item Technical conditions to construct moduli spaces
				\item Can still help classify unstable bundles
					\begin{itemize}
						\item Unstables are extensions of semistables (Harder-Narasimhan)
						\item Semistables are extensions of stables (Jordan-H\"older)
					\end{itemize}
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Harder-Narasimhan Filtration (Abelian Category)}
	%talk about Harder-Narasimhan, make Jordan-H\"older appear after and remark
	%it's not important
	\[
	\begin{tikzcd}
	0 \arrow[r, hook, color=blue]
		& E_1 \arrow[r, hook, color=red] \arrow[d, two heads, color=blue]
		& E_2 \arrow[r, hook, color=blue] \arrow[d, two heads, color=red]
		& \cdots \arrow[r, hook, color=red]
		& E_{n-1} \arrow[r, hook, color=blue] \arrow[d, two heads, color=red]
		& E_n \arrow[r, equal] \arrow[d, two heads, color=blue]
		& E
	\\
		\color{violet}
		\text{\hspace{-5em}\rlap{Semistable:}}
		& \color{violet} Q_1
		& \color{violet} Q_2
		&
		& \color{violet} Q_{n-1}
		& \color{violet} Q_n
		&
	\\
		& \mu({\color{violet} Q_1}) \arrow[r, ">" description, phantom]
		& \mu({\color{violet} Q_2}) \arrow[r, ">" description, phantom]
		& \cdots \arrow[r, ">" description, phantom]
		& \mu({\color{violet} Q_{n-1}}) \arrow[r, ">" description, phantom]
		& \mu({\color{violet} Q_n})
	\end{tikzcd}
	\]
	
	\pause
	\hrule
	\vspace{1em}
	
	Jordan H\"older Filtration
	
	\[
	\begin{tikzcd}
	0 \arrow[r, hook, color=blue]
		& F_1 \arrow[r, hook, color=red] \arrow[d, two heads, color=blue]
		& F_2 \arrow[r, hook, color=blue] \arrow[d, two heads, color=red]
		& \cdots \arrow[r, hook, color=red]
		& F_{m-1} \arrow[r, hook, color=blue] \arrow[d, two heads, color=red]
		& F_m \arrow[r, equal] \arrow[d, two heads, color=blue]
		& \color{violet} Q_n
	\\
		\color{purple}
		\text{\hspace{-3em}\rlap{Stable:}}
		& \color{purple} S_1
		& \color{purple} S_2
		&
		& \color{purple} S_{m-1}
		& \color{purple} S_m
		&
	\end{tikzcd}
	\]
\end{frame}

\begin{frame}[fragile]{Harder-Narasimhan Example}
	\[
	\begin{tikzcd}
	0 \arrow[r, hook, color=blue]
		& \bigO(n) \arrow[r, hook, color=red] \arrow[d, two heads, color=blue]
		& \bigO(n) \oplus \bigO(-n) \arrow[r, equal] \arrow[d, two heads, color=red]
		& \bigO(n) \oplus \bigO(-n)
	\\
		\color{violet}
		\text{\hspace{-5em}\rlap{\sout{Semi}stable:}}
		& \color{violet} \bigO(n)
		& \color{violet} \bigO(-n)
	\\
		& \mu({\color{violet} \bigO(n)}) = n \arrow[r, ">" description, phantom]
		& \mu({\color{violet} \bigO(-n)}) = -n
	\end{tikzcd}
	\]
\end{frame}

\begin{sagesilent}
from plots import MumfordCentralChargePlot
\end{sagesilent}

\section{Transition Triangulated Categories}
\begin{frame}{Central Charge for Mumford Stability}
	\[
		Z(E) = \rank(E) + \sqrt{-1} \cdot \degree(E)
	\]
	\vfill
	\sageplot[width=\linewidth]{MumfordCentralChargePlot()}
	\begin{columns}[T] % align columns
	\begin{column}{.48\linewidth}
		\[
			\centralcharge (E) = r(E) e^{i\pi \varphi(E)}
		\]
		\begin{center}
			\begin{large}
			$\varphi$ is "phase"
			\end{large}
		\end{center}
	\end{column}%
	%\hfill%
	\begin{column}{.48\linewidth}
		\[
			\mu(E) =
			\frac{
				- \realpart(\centralcharge(E))
			}{
				\imagpart(\centralcharge(E))
			}
			\quad
		\]
		\begin{center}
			\begin{large}
				(allow for $+\infty$)
			\end{large}
		\end{center}
	\end{column}%
	\end{columns}
\end{frame}

\begin{frame}{Extending Central Charge to $D^b(X)$}
	\[
		E^\bullet = [\cdots \to * \to * \to * \to \cdots] \in D^b(X)
	\]
	\begin{align*}
		\rank(E^\bullet) &= \sum (-1)^i \rank(\cohom^i(E)) \\
		\degree(E^\bullet) &= \sum (-1)^i \degree(\cohom^i(E))
	\end{align*}

	\vfill

	\begin{columns}[t,onlytextwidth]
		\begin{column}{.5\linewidth}
			In particular, for shifts:
			\begin{itemize}
				\item $\rank(E[1]) = - \rank(E)$
				\item $\degree(E[1]) = - \degree(E)$
			\end{itemize}
		\end{column}
		\begin{column}{.49\linewidth}
			For $\centralcharge$:
			\begin{itemize}
				\item $\centralcharge(E[1]) = - \centralcharge(E)$
				\item $\centralcharge(E[2]) = \centralcharge(E)$
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}

\begin{sagesilent}
from plots import NoShiftPhase, OneShiftPhase, TwoShiftPhase
\end{sagesilent}


\begin{frame}{Slicing - Phase $\phi$}
\begin{columns}[t,onlytextwidth] % align columns
	\begin{column}{.33\linewidth}%
	\resizebox{1.1\hsize}{!}{
		\sageplot{NoShiftPhase()}
	}
	\end{column}%
	%\hfill%
	\begin{column}{.33\linewidth}%
	\resizebox{1.1\hsize}{!}{
		\sageplot{OneShiftPhase()}
	}
	\end{column}%
	%\hfill%
	\begin{column}{.33\linewidth}%
	\resizebox{1.1\hsize}{!}{
		\sageplot{TwoShiftPhase()}
	}
	\end{column}%
	\end{columns}

	\vfill

	\begin{itemize}
		\item $\centralcharge(E[2]) = \centralcharge(E)$
		\item But $\varphi(E[n]) = \varphi(E) + n$
		\item Stability decided among $E \in D^b(X) \colon \varphi(E) \in (0, \pi] =
			\coh(X)$ \\
			($\varphi$ only partial ``function'')
	\end{itemize}
\end{frame}


\begin{frame}[fragile]{Harder-Narasimhan (Triangulated Category)}
	\[
	\begin{tikzcd}
	0 \arrow[r, color=blue]
		& E_1 \arrow[r, color=red] \arrow[d, color=blue]
		& E_2 \arrow[r, color=blue] \arrow[d, color=red]
		& \cdots \arrow[r, color=red]
		& E_{n-1} \arrow[r, color=blue] \arrow[d, color=red]
		& E_n \arrow[r, equal] \arrow[d, color=blue]
		& E
	\\
		\color{violet}
		%\text{\hspace{-5em}\rlap{Semistable:}}
		& \color{violet} Q_1 \arrow[lu, squigarrow, color=blue]
		& \color{violet} Q_2 \arrow[ul, squigarrow, color=red]
		& \cdots \arrow[ul, squigarrow, color=blue]
		& \color{violet} Q_{n-1} \arrow[ul, squigarrow, color=red]
		& \color{violet} Q_n \arrow[ul, squigarrow, color=blue]
		&
	\\
		& \varphi({\color{violet} Q_1}) \arrow[r, ">" description, phantom]
		& \varphi({\color{violet} Q_2}) \arrow[r, ">" description, phantom]
		& \cdots \arrow[r, ">" description, phantom]
		& \varphi({\color{violet} Q_{n-1}}) \arrow[r, ">" description, phantom]
		& \varphi({\color{violet} Q_n})
	\end{tikzcd}
	\]
	\hrule
	\[
	\begin{tikzcd}[opacity=0.3]
	0 \arrow[r, hook, color=blue]
		& E_1 \arrow[r, hook, color=red] \arrow[d, two heads, color=blue]
		& E_2 \arrow[r, hook, color=blue] \arrow[d, two heads, color=red]
		& \cdots \arrow[r, hook, color=red]
		& E_{n-1} \arrow[r, hook, color=blue] \arrow[d, two heads, color=red]
		& E_n \arrow[r, equal] \arrow[d, two heads, color=blue]
		& E
	\\
		\color{violet}
		\text{\hspace{-5em}\rlap{Semistable:}}
		& \color{violet} Q_1
		& \color{violet} Q_2
		&
		& \color{violet} Q_{n-1}
		& \color{violet} Q_n
		&
	\\
		& \mu({\color{violet} Q_1}) \arrow[r, ">" description, phantom]
		& \mu({\color{violet} Q_2}) \arrow[r, ">" description, phantom]
		& \cdots \arrow[r, ">" description, phantom]
		& \mu({\color{violet} Q_{n-1}}) \arrow[r, ">" description, phantom]
		& \mu({\color{violet} Q_n})
	\end{tikzcd}
	\]
\end{frame}

% example with two step complex

\section{Gains}
\begin{frame}{Benefits of the Triangulated Category View}
	\begin{itemize}
		\item Nothing is lost (stabilities on heart $\leftrightarrow$ stability on
			triangulated category)
		\item No strong stability conditions on $\coh(X)$ for $\dim(X) > 1$, but
			can exist for $D^b(X)$
		\item Studying derived category is popular
		\item Can make use of derived category machinery for stability problems
	\end{itemize}
\end{frame}

\end{document}
