# Requires GNU make, xargs, a latex distribution, sage
# and sagetex.sty visible in TEXINPUTS

MAINTEXFILE = main.tex
TEXFILES = ${MAINTEXFILE}
SAGETEXSCRIPT = main.sagetex.sage

main.pdf: ${TEXFILES}  main.sagetex.sout.tmp
	latexmk

main.sagetex.sout.tmp: ${SAGETEXSCRIPT} plots.py
	PYTHONPATH=./sagetexscripts/ sage ${SAGETEXSCRIPT}

${SAGETEXSCRIPT}: ${TEXFILES}
	latexmk || echo this shoud fail

plots.py: plots.ipynb
	jupyter nbconvert --to script plots.ipynb
	mv plots.py plots.sage
	sed -e "/get_ipython/d" -i plots.sage
	sage --preparse plots.sage
	mv plots.sage.py plots.py

.PHONY: clean nosage noappendix
clean:
	rm -rf **/__pycache__
	latexmk -C
	git clean -xf || echo no git repo to use for cleaning

nosage:
	latexmk

noappendix: ${TEXFILES}  main.sagetex.sout.tmp
	latexmk
